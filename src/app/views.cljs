(ns app.views
  (:require [app.state :refer [app-state]]
            [reagent.core :as reagent]
            [app.events :as events]
            ["semantic-ui-react" :as semantic-ui]
            [re-pressed.core :as rp]
            [re-frame.core :as re-frame]))

(enable-console-print!)

(defn component
    "Get a component from sematic-ui-react:
        (component \"Button\")"
  [k]
  (aget semantic-ui k))

(def card (component "Card"))
(def button (component "Button"))
(def icon (component "Icon"))
(def tab (component "Tab"))
(def tabpane (component "TabPane"))
(def container (component "Container"))
(def header (component "ListHeader"))
(def segment (component "Segment"))
(def uilist (component "List"))
(def item (component "ListItem"))
(def listcontent (component "ListContent"))

(defn feed []
  [:> segment {:class "inverted left aligned"}
   [:> uilist {:class "inverted"}
   (map-indexed
          (fn[i c]
            (let [{:keys [title content]} c]
              ^{:key title}
                   [:> item
                    [:> listcontent
                     {:id i}
                     [:> header (str i " : " title)]
                     (re-find #"[^<]*" content)
                     ]])
            )
     (:items @(re-frame/subscribe [::events/feed]))
     )
   ]])

(defn app []
  [feed])


(defonce read-rss (re-frame/dispatch [::events/http-post]))
(defonce reg-key (re-frame/dispatch-sync [::rp/add-keyboard-event-listener "keydown"]))

(re-frame/dispatch
     [::rp/set-keydown-rules
      {:event-keys [
                    [[::events/card-move-down]
                     ;; down arrow or s
                     [{:keyCode 40}]
                     [{:keyCode 74}]]

                    [[::events/card-move-up]
                     ;; up arrow or w
                     [{:keyCode 38}]
                     [{:keyCode 75}]]
                    ]
       }])


