(ns app.events
  (:require [app.state :refer [app-state]]
            [ajax.core :as ajax]
            [re-frame.core :as re-frame]
            [day8.re-frame.http-fx]
            ))

(enable-console-print!)

(re-frame/reg-event-fx
  ::http-post
  (fn [_world [_ val]]
    {:http-xhrio {:method          :get
		  :uri             "https://rss2json.com/api.json"
                  :params          {:rss_url "http://rss.slashdot.org/Slashdot/slashdotMain"}
		  :timeout         5000
		  :response-format (ajax/json-response-format {:keywords? true})
		  :on-success      [::good-http-result :hello]
		  :on-failure      [::bad-http-result]}}))

(re-frame/reg-event-db
  ::good-http-result
  (fn [db [_ hello result]]
    (assoc 
      (assoc db :api-result result)
      :index 1)
      ))

(re-frame/reg-sub
  ::feed
  (fn [db]
    (:api-result db)))

(re-frame/reg-event-db
 ::card-move-down
 (fn [db _]
   (println "down" (:index db))
   (let [nitems (count (:items (:api-result db)))]
     (.scrollIntoView (.getElementById js/document 
                        (str (mod (:index db) nitems))))
   (update-in db [:index] inc )
)))

(re-frame/reg-event-db
 ::card-move-up
 (fn [db _]
   (println "up" (:index db))
   (let [nitems (count (:items (:api-result db)))]
     (.scrollIntoView (.getElementById js/document 
                        (str (mod (:index db) nitems))))
   (update-in db [:index] dec ))))
